package com.example.myrecyclerview2021;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ActivityAjout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout);
    }

    public void validerProf(View view) {

        finish();
    }

    @Override
    public void finish(){
        EditText nom =findViewById(R.id.editTextName);
        EditText mat = findViewById(R.id.editTextMat);

        Prof prof = new Prof(nom.getText().toString(),mat.getText().toString());
        Intent intent = new Intent();
        intent.putExtra("prof",prof);
        setResult(Activity.RESULT_OK, intent);
        super.finish();
    }
}