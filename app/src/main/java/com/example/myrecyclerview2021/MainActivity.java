package com.example.myrecyclerview2021;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Prof> lesProfs = new ArrayList<Prof>();
    private LinearLayoutManager linearLayoutManager;
    private MyAdapter myAdapter;
    private Parcelable recyclerState;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        if (savedInstanceState !=null){
            Log.d("MesLogs","recuperation des profs");
            lesProfs = savedInstanceState.getParcelableArrayList("donnees");
        }
        if (sharedPreferences.contains("lesprofs")){
            lesProfs = new Gson().fromJson(sharedPreferences.getString("lesprofs",null),
                    new TypeToken<List<Prof>>(){}.getType());
        }
        myAdapter = new MyAdapter(lesProfs);
        recyclerView.setAdapter(myAdapter);
    }

    public void ajouter(View view) {
        Intent intent = new Intent(this.getApplicationContext(),ActivityAjout.class);
        startActivityForResult(intent,1);
    }

    @Override
    public void onActivityResult(int requestcode,int resultcode, Intent intent){
        super.onActivityResult(requestcode,resultcode,intent);
        Prof p = intent.getParcelableExtra("prof");
        lesProfs.add(p);
        myAdapter.notifyDataSetChanged();
    }

    protected void onSaveInstanceState(Bundle state){
        super.onSaveInstanceState(state);
        recyclerState = linearLayoutManager.onSaveInstanceState();
        state.putParcelable("infos",recyclerState);
        state.putParcelableArrayList("donnees",lesProfs);
        Log.d("MesLogs","sauvegarde des profs");
    }

    protected void onRestoreInstanceState(Bundle state){
        super.onRestoreInstanceState(state);
        if (state!=null){
            recyclerState = state.getParcelable("infos");
            lesProfs = state.getParcelableArrayList("donnees");
            myAdapter.setProfs(lesProfs);
            myAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (recyclerState !=null){
            linearLayoutManager.onRestoreInstanceState(recyclerState);
        }
        myAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause(){
        super.onPause();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("lesprofs",new Gson().toJson(lesProfs)).apply();
    }

    
}































