package com.example.myrecyclerview2021;

import android.view.View;

public interface ItemLongClickListener {
    void onItemLongClick(View v, int pos);
}
