package com.example.myrecyclerview2021;

import android.os.Parcel;
import android.os.Parcelable;

public class Prof implements Parcelable {
    private String nom;
    private String matiere;

    public Prof(String nom, String matiere) {
        this.nom = nom;
        this.matiere = matiere;
    }

    protected Prof(Parcel in) {
        nom = in.readString();
        matiere = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeString(matiere);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Prof> CREATOR = new Creator<Prof>() {
        @Override
        public Prof createFromParcel(Parcel in) {
            return new Prof(in);
        }

        @Override
        public Prof[] newArray(int size) {
            return new Prof[size];
        }
    };

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }
}
